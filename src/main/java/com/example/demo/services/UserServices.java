package com.example.demo.services;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServices {

    @Autowired
    private UserRepository userRepository;

    public User createUser(User user) {
        return userRepository.save(user);
    }

    public List<User> createUserList(List<User> list) {
        return userRepository.saveAll(list);
    }

    public List<User> getUserList() {
        return userRepository.findAll();
    }

    public User getUserById(int id) {
        return userRepository.findById(id).orElse(null);
    }

    public User updateUserById(int id, User user) {
        Optional<User> userFound = userRepository.findById(id);

        if (userFound.isPresent()) {
            User userUpdate = userFound.get();
            if (user.getFname() != "") {
                userUpdate.setFname(user.getFname());
            }

            if (user.getLname() != "") {
                userUpdate.setLname(user.getLname());
            }

            if (user.getAge() >= 0) {
                userUpdate.setAge(user.getAge());
            }



            return userRepository.save(user);
        } else {
            return null;
        }
    }

    public String deleteUserById(int id) {
        userRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
